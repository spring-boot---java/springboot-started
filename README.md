# springBoot-Started

 spring boot started

**Add:**

*Request:*
POST request http://localhost:8080/example/user

JSON Parameters
{
    "name": "Dany",
    "email": "Dany@hotmail.com"
}

*Response:*

{
    "id": 7,
    "name": "Dany",
    "email": "dany@hotmail.com"
}

**List:**

*Request:*
GET request http://localhost:8080/example/user

*Response:*
[
    {
        "id": 1,
        "name": "dany",
        "email": "dany@hotmail.com"
    }
]


