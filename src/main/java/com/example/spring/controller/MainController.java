package com.example.spring.controller;

import com.example.spring.model.User;
import com.example.spring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping(path = "/example")
public class MainController {
    @Autowired
    private UserRepository userRepository;

    @PostMapping(path = "/user")
    public  ResponseEntity addNewUser(@Valid @RequestBody User user){
        userRepository.save(user);
        return new ResponseEntity(user, HttpStatus.CREATED);
    }
    @GetMapping(path = "/user")
    public @ResponseBody Iterable<User> getAllUser(){

        return userRepository.findAll();
    }
    @PutMapping(path = "/user")
    public ResponseEntity updateUser(@Valid @RequestBody User user){
        userRepository.save(user);
        return new ResponseEntity(user, HttpStatus.OK);
    }
}
